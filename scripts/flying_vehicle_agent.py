#!/usr/bin/env python
import numpy as np
from numpy import *
from numpy import linalg as la
import rospy
import math
from scipy.stats import norm
from std_msgs.msg import String
from std_msgs.msg import Float32MultiArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from multi_agent_flight.msg import State2D
from multi_agent_flight.msg import State2DArray

class System(object):
	def __init__(self, ainit, binit, cinit, dinit, xinit):
		self.a = ainit
		self.b = binit
		self.c = cinit
		self.d = dinit
		self.xkm = xinit
		self.xk = xinit

	def printSystem(self):
		print("a = ")
		print(self.a)
		print("b = ")
		print(self.b)
		print("c = ")
		print(self.c)
		print("d = ")
		print(self.d)	

	def stateTrans(self, uk):
		self.xkm = self.xk
		self.xk = self.a*self.xkm + self.b*uk

class Agent(object):
	def __init__(self, idnum):
		self.ident = idnum
		self.role = 'none'
		#enable (1) or disable (0) fault detection
		self.fault_detect = 1
		self.res_thresh = 2.0
		self.agentlist = []
		self.agentroles = dict()
		self.agentpose = State2D()
		self.poseList = dict()
		self.velList = dict()	
		self.accList = dict()
		self.r = dict()
		self.start_time = 0.0
		self.fault = False
		self.fault_timer = 50
		self.fault_list = list()

		#fault probability distributions
		self.r_nf = norm(loc=0.0, scale=0.5)  # no-fault residual distribution
		self.r_fp = norm(loc=0.0, scale=5.0)  # fault-present residual distribution
		self.p = 0.0001   # prior probability of agent failure

		#sensor states
		self.r_rel = dict()
		self.v_rel = dict()
		self.a_rel = dict()
		self.r_abs = matrix( [[0],[0]] )
		self.v_abs = matrix( [[0],[0]] )
		self.a_abs = matrix( [[0],[0]] )
		self.group_target = matrix( [[0],[0]] )

		#position and velocity references <stub>
		self.r_ref = matrix( [[0],[0]] )
		self.v_ref = matrix( [[0],[0]] )
		self.uk = matrix( [[0],[0]] )
		
		#define formation
		#this will be updated to be recieved from external command
		self.formation = dict()
		self.formation['2'] = matrix( [[1.0], [-1.0]] )
		self.formation['3'] = matrix( [[-1.0], [-1.0]] )
		self.formation['4'] = matrix( [[0.0], [-2.0]] )

		rospy.init_node("agent_" + str(self.ident), anonymous=True)

		#Subscribers
		rospy.Subscriber('agents/ident', String, self.callback0)
		rospy.Subscriber('ext/cmd/target', State2D, self.callback1)
		rospy.Subscriber('env/from/rel/'+str(self.ident), State2DArray, self.callback2)
		rospy.Subscriber('env/from/abs/'+str(self.ident), State2D, self.callback4)
		
		#Publishers
		self.pub0 = rospy.Publisher('agents/ident', String)
		self.pub1 = rospy.Publisher('env/to/'+str(self.ident), State2D)
		self.pub3 = rospy.Publisher('agent/r/'+str(self.ident), Float32MultiArray)
		#define the system model
		self.dt = 0.02
		a = matrix( [[1, 0, self.dt, 0], [0, 1, 0, self.dt], [0, 0, 1, 0], [0, 0, 0, 1]] )
		b = matrix( [[0, 0], [0, 0], [self.dt, 0], [0, self.dt]] )
		c = matrix( [[1, 0, 0, 0], [0, 1, 0, 0]] )
		d = zeros( (2,2) )
		self.G = System(a, b, c, d, matrix( [[5*np.random.rand(1,1)], [5*np.random.rand(1,1)], [0], [0]] ) )
		#feedback control gain, velocity error to acceleration command
		self.K = matrix( [[1 ,0], [0, 1]] )
		#control gain
		self.k_v = 0.25
		#initial position sensor value
		#self.r_dir = matrix( [[0],[0]] )]
		self.run()

	def run(self):
		#assign initial roles
		self.announceId()
		rospy.sleep(1.0)
		self.compileList()
		rospy.sleep(1.0)
		#either publish as director to other agents or subscribe to director
		if (self.role == 'director'):
			self.pub2 = dict()
			for agent in self.agentlist:
				if not self.agentroles[agent] == 'director':
					self.pub2[agent] = rospy.Publisher('dir/pose/'+agent, State2D)
			rospy.Subscriber('ext/cmd/form', State2DArray, self.callback5)
		else:
			rospy.Subscriber('dir/pose/'+str(self.ident), State2D, self.callback3)
		#then enter loop
		r = rospy.Rate(1/self.dt) # 100hz
		rospy.loginfo("Starting Loop...")
		self.start_time = rospy.Time.now().to_sec()
		while not rospy.is_shutdown():
			#publish position to environment
			self.agentpose.x = self.G.xk[0,0]
			self.agentpose.y = self.G.xk[1,0]
			self.agentpose.vx = self.G.xk[2,0]
			self.agentpose.vy = self.G.xk[3,0]
			self.agentpose.ax = self.uk[0,0]
			self.agentpose.ay = self.uk[1,0]
			self.agentpose.agent = str(self.ident)
			self.pub1.publish(self.agentpose)
			self.processMotion(self.uk)
        		r.sleep()
			#rospy.spinonce()

	def compileList(self):
		self.agentlist = list(set(self.agentlist))
		minlist = min(self.agentlist)
		for x in self.agentlist:
			if x == minlist:
				self.agentroles[x] = 'director'
			else:
				self.agentroles[x] = 'agent'
		self.role = self.agentroles[str(self.ident)]
		rospy.loginfo(rospy.get_name() + ": I am %s" % self.role)

	def announceId(self):
		rospy.sleep(1.0)
		self.pub0.publish(str(self.ident))
		rospy.sleep(1.0)
		self.pub0.publish(str(self.ident))
		#rospy.sleep(1.0)
		#self.pub0.publish(str(self.ident))

	def processMotion(self, control):
		self.G.stateTrans(control)

	def processControl(self, agent, pos, vel):
		if (self.fault):
			return matrix( [[0], [0]] )
		else:
			u = matrix( [[0], [0]] )
			v = matrix( [[0], [0]] )
			c = 1.0
			d = 1.0
			phi = 2.0
			for j in self.agentlist:
				if (j != agent and j in self.poseList):
					s = phi*(la.norm(self.poseList[j] - pos) - d)/la.norm(self.poseList[j] - pos)
					v = v + s*(self.poseList[j] - pos) + c*(self.velList[j] - vel)
			v = v + self.k_v*(self.group_target - pos)
			u = self.K*(v - vel)
			#u = self.K*v
			return u

	def monitorAgents(self):
		if (rospy.Time.now().to_sec() - self.start_time > 5.0 and not self.fault):
			#plist = self.agentlist.copy()
			resid = Float32MultiArray()
			self.fault_timer = max(self.fault_timer - 1,0)
			alpha = 0.98  #1-second time constant
			#rospy.loginfo("Monitoring: " + str( set(self.agentlist) ))
			#rospy.loginfo("Monitoring: " + str( set(self.poseList.keys()) ))
			for i in set(self.agentlist).intersection( set(self.poseList.keys()) ):
				#compute their reasonable control input			
				ur = self.processControl(i, self.poseList[i], self.velList[i])
				#compute actual control input
				ua = self.accList[i]
				
				#compute residual
				if (i in self.r):
					self.r[i] = alpha*self.r[i] + (1 - alpha)*la.norm(ur - ua)
				else:
					self.r[i] = la.norm(ur - ua)
				resid.data.append(self.r[i])
				#fault detection - threshold based
				if (self.fault_detect == 1):
					#use threshold
					#if (self.r[i] > self.res_thresh or math.isnan(self.r[i])):
					#use probability
					if (self.computeResidualFault(self.r[i]) > 0.5):
						#if this is a new failure, reset timer
						if (i not in self.fault_list):
							self.fault_timer = 50
							self.fault_list.append(i)
			#resolve faults if no faults for 1 second
			if (self.fault_timer == 0):
				p = self.computeFaultProbability(len(self.agentlist) - len(self.fault_list),len(self.fault_list))
				if (p > 0.5):
					self.fault = True
					rospy.loginfo("I have a fault: " + str(p))
				else:
					for i in self.fault_list:
						#then remove failed agents from agent lists
						rospy.loginfo("Fault: Agent "+i)	
						self.agentlist.remove(i)
						del self.poseList[i]
						#self.velList.pop(i)
						#self.accList.pop(i)
				self.fault_list = []
			self.pub3.publish(resid)
			#rospy.loginfo('Monitoring Agents')
		else: return 0
		

	def computePositions(self):
		#determine absolute positions of neighbors
		#self.poseList_km = self.poseList
		#self.velList_km = self.velList
		for i in self.r_rel:
			self.poseList[i] = self.r_abs + self.r_rel[i]
			self.velList[i] = self.v_abs + self.v_rel[i]
			self.accList[i] = self.a_abs + self.a_rel[i]

	def determineRoles(self):
		return 0

	def computeFaultProbability(self, n, m):
		pp = 0.001;
		alpha = 1 - 0.0001;
		beta = 1 - 0.0001;
		p = (alpha**m * (1 - beta)**n)*pp/(pp*(1 - beta)**n + pp**m + (1 - alpha)**m)
		return p

	def computeResidualFault(self, resid):
		#probability of a fault given a residual value
		return self.r_fp.pdf(resid)*self.p/(self.p*self.r_fp.pdf(resid) + (1 - self.p)*self.r_nf.pdf(resid))

	def assignAgents(self, agentlist):
		agentlist.sort()

	def callback0(self, data):
		# get ident from other agents
		rospy.loginfo(rospy.get_name() + ": I heard %s" % data.data)
		self.agentlist.append(data.data)

	def callback1(self, data):
		# external commands
		#rospy.loginfo("Received Target")
		self.group_target = matrix( [[data.x],[data.y]] )
		self.r_ref = self.group_target

	def callback2(self, data):
		# sensor data from environment
		# data contains list of relative positions of other agents
		for i in data.states:
			self.r_rel[i.agent] = matrix( [[i.x],[i.y]] )
			self.v_rel[i.agent] = matrix( [[i.vx],[i.vy]] )
			self.a_rel[i.agent] = matrix( [[i.ax],[i.ay]] )
		self.computePositions()
		self.uk = self.processControl(str(self.ident), self.r_abs, self.v_abs)
		self.monitorAgents()

	def callback4(self, data):
		# sensor data from environment
		# data contains global position
		self.r_abs = matrix( [[data.x],[data.y]] )
		self.v_abs = matrix( [[data.vx],[data.vy]] )
		self.a_abs = matrix( [[data.ax],[data.ay]] )

	def callback3(self, data):
		#only received if not director
		#rospy.loginfo(str(self.ident) + " Recieved Commands")
		self.r_ref = matrix( [[data.x],[data.y]] )
		self.v_ref = matrix( [[data.vx],[data.vy]] )		

	def callback5(self, data):
		#get formation, PoseArray
		#rospy.loginfo("Received Formation")
		#self.formation = dict()
		i = 1;
		for formpose in data.states:
			self.formation[str(i)] = matrix( [[formpose.x], [formpose.y]] )
			i = i + 1
		
	

if __name__ == '__main__':
	print("Starting")
	a = Agent(1)
	rospy.spin()
		
