#!/usr/bin/env python
import rospy
import math
from std_msgs.msg import String
from geometry_msgs.msg import Pose
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from multi_agent_flight.msg import State2D
from multi_agent_flight.msg import State2DArray
from scipy.stats import norm

class Environment(object):
	def __init__(self):
		self.agentlist = []
		self.faillist = dict()
		rospy.init_node("environment", anonymous=True)
		rospy.Subscriber('agents/ident', String, self.callback0)
		rospy.sleep(5.0)
		#create list of all agents in environment by listening to identification messages
		self.compileList()
		#subscribe to topic for each agent in list
		#create publisher for each agent in list
		self.pub1 = dict()
		self.pub2 = dict()
		self.markerpub = dict()
		self.distance = {}
		self.posedata = {}
		self.comm_range = 1.1
		#setup visualization of agents
		self.marker = dict()
		for agent in self.agentlist:
			self.setupMarker(agent)
		self.markerpub = rospy.Publisher('env/markers/', MarkerArray)
		for agent in self.agentlist:
			self.pub1[agent] = rospy.Publisher('env/from/rel/'+agent, State2DArray)
			self.pub2[agent] = rospy.Publisher('env/from/abs/'+agent, State2D)
		for agent in self.agentlist:
			rospy.Subscriber('env/to/'+agent, State2D, self.callback1)
		self.run()
	
	def run(self):
		r = rospy.Rate(20) # 100hz
		n = 0
		while not rospy.is_shutdown():
			n = n + 1
			self.publishMarkers()
			if (n == 400):
				self.faillist['2'] = 1
				#self.faillist['3'] = 1
        		r.sleep()
			#rospy.spin()

	def callback0(self, data):
		rospy.loginfo(rospy.get_name() + ": I heard %s" % data.data)
		self.agentlist.append(data.data)

	def callback1(self, data):
		#process position sensor data
		self.posedata[data.agent] = data
		sensordata = State2DArray()
		#sensordata.append(data)  #attach own agents absolute positition (to do:  add sensor noise)
		#determine closeness matrix of which agents recieve messages from which other agents
		for i in self.agentlist:
			#d = self.agentDistance(name, i)
			#self.distance[name,i] = d
			#self.distance[i,name] = d
			#determine which agents it should be sent to and publish
			#send relative position of director to each agent
			if (i in self.posedata):
				relpos = State2D()
				if (self.faillist[data.agent] == 2):
					relpos.x = 1
					relpos.y = 1
					relpos.vx = 1
					relpos.vy = 1
					relpos.agent = i
				elif (self.faillist[data.agent] == 3):
					noise = 100.0
					relpos.x = relpos.x + norm.rvs(0.0, 1, 1)*noise
					relpos.y = relpos.y + norm.rvs(0.0, 1, 1)*noise
					relpos.vx = relpos.vx + norm.rvs(0.0, 1, 1)*noise
					relpos.vy = relpos.vy + norm.rvs(0.0, 1, 1)*noise
				else:
					relpos.x = self.posedata[i].x - data.x
					relpos.y = self.posedata[i].y - data.y
					relpos.vx = self.posedata[i].vx - data.vx
					relpos.vy = self.posedata[i].vy - data.vy
					relpos.agent = i
				sensordata.states.append(relpos)
		self.pub1[data.agent].publish(sensordata)
		#send absolute position to each agent
		if (self.faillist[data.agent] == 1):
			pos = State2D()
			pos.x = 0
			pos.y = 0
			pos.vx = 0
			pos.vy = 0
			pos.ax = 0
			pos.ay = 0
		elif (self.faillist[data.agent] == 4):
			pos = State2D()
			noise = 3
			pos.x = data.x + norm.rvs(0.0, 1, 1)*noise
			pos.y = data.y + norm.rvs(0.0, 1, 1)*noise
			pos.vx = data.vx + norm.rvs(0.0, 1, 1)*noise
			pos.vy = data.vy + norm.rvs(0.0, 1, 1)*noise
			pos.ax = data.ax + norm.rvs(0.0, 1, 1)*noise
			pos.ay = data.ay + norm.rvs(0.0, 1, 1)*noise
		else:
			pos = data
		self.pub2[data.agent].publish(pos)

	def compileList(self):
		self.agentlist = list(set(self.agentlist))
		for agent in self.agentlist:
			self.faillist[agent] = 0

	def agentDistance(self, agent1, agent2):
		if (agent1 in self.posedata and agent2 in self.posedata):
			delx = self.posedata[agent2].x - self.posedata[agent1].x
			dely = self.posedata[agent2].y - self.posedata[agent1].y
			return math.sqrt(delx*delx + dely*dely)
		else: return 0

	def setupMarker(self, agent):
		_id=0
		_type = Marker.CUBE

		tmarker = Marker(type=_type, action=Marker.ADD)
		##marker.header.frame_id = point.header.frame_id
		tmarker.header.frame_id = 'my_frame'
		tmarker.header.stamp = rospy.Time.now()
		tmarker.pose.position.x = 0.0
		tmarker.pose.position.y = 0.0
		tmarker.pose.position.z = 0.0

		tmarker.ns = 'agent/' + str(agent);
		tmarker.id = int(agent);

		#pointxyz
		tmarker.pose.orientation.x = 0.0
		tmarker.pose.orientation.y = 0.0
		tmarker.pose.orientation.z = 0.0
		tmarker.pose.orientation.w = 1

		tmarker.scale.x = 0.25
		tmarker.scale.y = 0.25
		tmarker.scale.z = 0.1

		tmarker.color.r = 0
		tmarker.color.g = 1
		tmarker.color.b = 0
		tmarker.color.a = 1
		tmarker.id = _id
		self.marker[agent] = tmarker

	def publishMarkers(self):
		for agent in self.agentlist:
			if agent in self.posedata:
				self.marker[agent].pose.position.x = self.posedata[agent].x
				self.marker[agent].pose.position.y = self.posedata[agent].y
				if (self.faillist[agent] != 0):
					self.marker[agent].color.r = 1
					self.marker[agent].color.g = 0
		temp = MarkerArray()
		for i in self.marker:
			temp.markers.append(self.marker[i])
		self.markerpub.publish(temp)

class Agent(object):
	def __init__(self, idnum, position):
		return 0

if __name__ == '__main__':
	env = Environment()


