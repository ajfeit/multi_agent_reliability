#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from multi_agent_flight.msg import State2D
from multi_agent_flight.msg import State2DArray

def getForm(n):
	formmsg = State2DArray()
	formtemp = []
	formtemp = form[n]
	for loc in formtemp:
		temp = State2D()
		temp.x = loc[0]
		temp.y = loc[1]
		formmsg.states.append(temp)
	return formmsg
		

if __name__ == '__main__':
	print("Starting External Command")
	rospy.init_node("ext_cmd", anonymous=True)
	pub0 = rospy.Publisher('ext/cmd/form', State2DArray)
	pub1 = rospy.Publisher('ext/cmd/target', State2D)
	pos_cmd = State2D()
	pos_cmd.x = 0
	pos_cmd.y = 0
	form = dict()
	form[1] = [ [0,0], [0,-1], [0,-2], [0,-3] ]
	form[2] = [ [0,0], [-1, -1], [1, -1], [0, -2] ]
	position = dict()
	position[1] = [-5, -5]
	position[2] = [-5, 5]
	position[3] = [5, -5]
	position[4] = [5, 5]
	time = rospy.Time.now()
	formn = 1
	posn = 1
    	while not rospy.is_shutdown():
		#rospy.loginfo("External Command Running")
		pos_cmd.x = position[posn][0]
		pos_cmd.y = position[posn][1]
		pub1.publish(pos_cmd)
        	pub0.publish(getForm(formn))
		if ((rospy.Time.now() - time).to_sec() > 20):
			rospy.loginfo("Changing Formation")
			time = rospy.Time.now()
			if (formn == 1):
				formn = 2
			else: formn = 1
			posn = posn + 1
			if (posn > 4): posn = posn - 4
        	rospy.sleep(1.0)
		#rospy.spin()
